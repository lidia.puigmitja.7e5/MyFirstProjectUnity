using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrigerDetection : MonoBehaviour
{
    public GameObject SetaPrefab;
    public float _minRandom, maxRandom, _maximumY, _minimumY;
    // Start is called before the first frame update
    void Start()
    {
        _minRandom = -5f;
        maxRandom = 5f;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Debug.Log("me choqu�!");
            collision.gameObject.GetComponent<PlayerData>().SetSeta();
            Vector3 newPos = new Vector3(Random.Range(_minRandom, maxRandom),SetaPrefab.transform.position.y, 0);
            Instantiate(SetaPrefab, newPos, Quaternion.identity);
            Destroy(this.gameObject); //Destuir en memoria
            //this.gameObject.SetActivefalse); no elimina en memoria
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
