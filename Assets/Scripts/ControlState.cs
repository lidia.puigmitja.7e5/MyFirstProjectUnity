using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlState : MonoBehaviour
{
    
    private Vector3 _minimScale = new Vector3(2.5625f, 2.5625f,1f);
    private Vector3 _maximScale = new Vector3(7.5625f, 7.5625f, 7f);
    private Vector3 _changeScale = new Vector3(0.01f, 0.01f, 0.01f);
    private PlayerData _playerData;
    private GameObject _player;
    private Button _botton;


    // Start is called before the first frame update
    void Start()
    {
        _playerData = GameObject.Find("Player").GetComponent<PlayerData>();
        _player = GameObject.Find("Player");

    }

    // Update is called once per frame
    void Update()
    {
        _botton = GameObject.Find("GrowUp").GetComponent<Button>();
        _botton.onClick.AddListener(TaskOnClick);
        ChangeState();
        
    }

    private void TaskOnClick()
    {
        if (_playerData.Player_state == PlayerState.Normal)
        {
            _playerData.Player_state = PlayerState.Crecendo;
        }else if (_playerData.Player_state == PlayerState.Gigant)
        {
            _playerData.Player_state = PlayerState.Decrecendo;
        }
    }

    private void ChangeState()
    {
       
        switch (_playerData.Player_state)
        {
            case PlayerState.Normal:
                _player.transform.localScale = _minimScale;
                break;

            case PlayerState.Gigant:
                _player.transform.localScale = _maximScale;
                break;

            case PlayerState.Crecendo:
                if(_player.transform.localScale != _maximScale) _player.transform.localScale += _changeScale;
             
                break;

            case PlayerState.Decrecendo:
                if (_player.transform.localScale != _maximScale) _player.transform.localScale -= _changeScale;
                break;


        }

    }

}
