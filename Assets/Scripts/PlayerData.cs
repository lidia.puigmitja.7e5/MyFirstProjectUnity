using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum PlayerState{
    Gigant,
    Normal,
    Crecendo,
    Decrecendo,

}



public class PlayerData : MonoBehaviour
{

    public PlayerState Player_state;
    //Nom, Tipus de personatge, al�ada, velocitat, dist�ncia a rec�rrer. Aquest component ha de ser consultat per altres classes i editable des de l'inspector.
    public string PlayerName;
    public float Height = 1.50f;
    [SerializeField]
    private int _yearsOld = 20;
    // public int[] Stats;
    [SerializeField]
    private Sprite[] _sprite;
    private SpriteRenderer _sr;
    private int _frameRate;
    private int _counter;
    private float _speedAtenuador = 0.005f;
    private int _i = 0;
    private int seta;

    public void SetSeta()
    {
        seta += 1;
    }
    public int GetSeta()
    {
        return seta;
    }

    public void SetSeta(int p) //sobreCarrega
    {
        seta += 1;
    }



    // Start is called before the first frame update
    void Start()
    {
        _sr = GetComponent<SpriteRenderer>();
        _counter = 0;
        _frameRate = 100;
    }

    // Update is called once per frame
    void Update()
    {

        transform.position = new Vector3(_speedAtenuador*Input.GetAxis("Horizontal")+transform.position.x, _speedAtenuador * Input.GetAxis("Vertical")+transform.position.y, transform.position.z);
        
        if (_counter >= _frameRate)
        {
            _counter = 0;
            this.gameObject.GetComponent<SpriteRenderer>().sprite = _sprite[_i];
            _i = _i < _sprite.Length - 1 ? (_i + 1) : 0;

        }
        else
        {
            _counter++;
        }



    }

    }


