using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CambioSceneName : MonoBehaviour
{
    private Text _player;
    private PlayerData _playerData;
    private Button _botton;
    // Start is called before the first frame update
    void Start()
    {

            _botton = GameObject.Find("Bitcoin").GetComponent<Button>();
            _botton.onClick.AddListener(TaskOnClick);
        

    }

    void TaskOnClick()
    {
        _player = GameObject.Find("Name").GetComponent<Text>();
        
   
        if (_player.text.Length!=0)
        {
            
            PlayerPrefs.SetString("name", _player.text);
            SceneManager.LoadScene("SampleScene");
        }
     
     
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
