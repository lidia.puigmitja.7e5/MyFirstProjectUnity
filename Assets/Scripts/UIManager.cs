using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private GameObject _player;
    private Button _botton;
    private GameObject _panel;
    private GameObject _floor;
    private GameObject _kuadrat;

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player");
        _floor = GameObject.Find("Floor");
        _kuadrat = GameObject.Find("Cuadrao");
        _kuadrat.SetActive(false);
        _floor.SetActive(false);
        _player.SetActive(false);
        _botton = GameObject.Find("Bitcoin").GetComponent<Button>();
        _panel = GameObject.Find("GameStart");
        _botton.onClick.AddListener(TaskOnClick);
        
       
    }


    // Update is called once per frame
    void Update()
    {
      
    }


    void TaskOnClick()
    {
        _player.SetActive(true);
        _floor.SetActive(true);
        _kuadrat.SetActive(true);
        _panel.SetActive(false);
    }
   
}
